package com.example.progmob_2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HapusMahasiswaActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_mahasiswa);

        EditText edNim = (EditText)findViewById(R.id.editNim);
        Button btnHapus = (Button)findViewById(R.id.btnHapus);
        pd = new ProgressDialog(HapusMahasiswaActivity.this);

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_mhs(
                        edNim.getText().toString(),
                        "72180250"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(HapusMahasiswaActivity.this,"Berhasil di Hapus", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(HapusMahasiswaActivity.this,LihatMahasiswaActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(HapusMahasiswaActivity.this, "Gagal", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}