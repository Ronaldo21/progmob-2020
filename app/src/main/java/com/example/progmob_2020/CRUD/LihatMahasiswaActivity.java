package com.example.progmob_2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.example.progmob_2020.Adapter.MahasiswaCRUDRecycleAdapter;
import com.example.progmob_2020.Model.MahasiswaCRUD;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LihatMahasiswaActivity extends AppCompatActivity {
    RecyclerView rvMhs;
    MahasiswaCRUDRecycleAdapter mhsAdapter;
    ProgressDialog pd;
    List<MahasiswaCRUD> mahasiswaList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_mahasiswa);
        rvMhs = (RecyclerView)findViewById(R.id.rvGetMahasiswa);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<MahasiswaCRUD>> call = service.getMahasiswa("72180250");

        call.enqueue(new Callback<List<MahasiswaCRUD>>() {
            @Override
            public void onResponse(Call<List<MahasiswaCRUD>> call, Response<List<MahasiswaCRUD>> response) {
                pd.dismiss();
                mahasiswaList = response.body();
                mhsAdapter = new MahasiswaCRUDRecycleAdapter(mahasiswaList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(LihatMahasiswaActivity.this);
                rvMhs.setLayoutManager(layoutManager);
                rvMhs.setAdapter(mhsAdapter);
            }

            @Override
            public void onFailure(Call<List<MahasiswaCRUD>> call, Throwable t) {
                Toast.makeText( LihatMahasiswaActivity.this,"Error", Toast.LENGTH_LONG).show();

            }
        });
    }
}