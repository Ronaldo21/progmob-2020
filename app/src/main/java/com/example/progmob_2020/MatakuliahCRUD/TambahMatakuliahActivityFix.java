package com.example.progmob_2020.MatakuliahCRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahMatakuliahActivityFix extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_matakuliah_fix);

        EditText edNama = (EditText)findViewById(R.id.editTextNamaMatkulFix);
        EditText edKode = (EditText)findViewById(R.id.editTextKodeMatkulFix);
        EditText edHari = (EditText)findViewById(R.id.editTextHariMatkulFix);
        EditText edSesi = (EditText)findViewById(R.id.editTextSesiMatkulFix);
        EditText edSks = (EditText)findViewById(R.id.editTextSksMatkulFix);
        Button btnSimpan = (Button)findViewById(R.id.btnTambahMatkulFix);
        pd = new ProgressDialog(TambahMatakuliahActivityFix.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Loading");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_Matakuliah(
                        edNama.getText().toString(),
                        edKode.getText().toString(),
                        edHari.getText().toString(),
                        edSesi.getText().toString(),
                        edSks.getText().toString(),
                        "72108250"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahMatakuliahActivityFix.this,"Data Berhasil Disimpan",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(TambahMatakuliahActivityFix.this, LihatMatakuliahActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(TambahMatakuliahActivityFix.this,"Gagal", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}