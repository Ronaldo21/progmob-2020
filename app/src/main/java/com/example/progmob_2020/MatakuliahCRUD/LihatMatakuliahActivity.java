package com.example.progmob_2020.MatakuliahCRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.example.progmob_2020.Adapter.MahasiswaCRUDRecycleAdapter;
import com.example.progmob_2020.Adapter.MatakuliahCRUDRecycleAdapter;
import com.example.progmob_2020.CRUD.LihatMahasiswaActivity;
import com.example.progmob_2020.Model.MahasiswaCRUD;
import com.example.progmob_2020.Model.MatakuliahCRUD;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LihatMatakuliahActivity extends AppCompatActivity {
    RecyclerView rvMatakuliah;
    MatakuliahCRUDRecycleAdapter matkulAdapter;
    ProgressDialog pd;
    List<MatakuliahCRUD> matakuliahList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_matakuliah);

        rvMatakuliah = (RecyclerView)findViewById(R.id.rvGetMatakuliah);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<MatakuliahCRUD>> call = service.getMatakuliah("72180250");

        call.enqueue(new Callback<List<MatakuliahCRUD>>() {
            @Override
            public void onResponse(Call<List<MatakuliahCRUD>> call, Response<List<MatakuliahCRUD>> response) {
                pd.dismiss();
                matakuliahList = response.body();
                matkulAdapter = new MatakuliahCRUDRecycleAdapter(matakuliahList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(LihatMatakuliahActivity.this);
                rvMatakuliah.setLayoutManager(layoutManager);
                rvMatakuliah.setAdapter(matkulAdapter);
            }

            @Override
            public void onFailure(Call<List<MatakuliahCRUD>> call, Throwable t) {
                Toast.makeText( LihatMatakuliahActivity.this,"Error", Toast.LENGTH_LONG).show();

            }
        });

    }
}