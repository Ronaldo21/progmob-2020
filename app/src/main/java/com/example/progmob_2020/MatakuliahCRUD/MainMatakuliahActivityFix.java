package com.example.progmob_2020.MatakuliahCRUD;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob_2020.R;

public class MainMatakuliahActivityFix extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_matakuliah_fix);
        Button btnLihatMatkul = (Button)findViewById(R.id.btnLihatMatakuliahFix);
        Button btnTambahMatkul = (Button)findViewById(R.id.btnTambahMatakuliahFix);
        Button btnHapusMatkul = (Button)findViewById(R.id.btnHapusMatakuliahFix);
        Button btnUpdateMatkul = (Button)findViewById(R.id.btnUpdateMatakuliahFix);

        btnLihatMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatakuliahActivityFix.this, LihatMatakuliahActivity.class);
                startActivity(intent);
            }
        });

        btnTambahMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatakuliahActivityFix.this, TambahMatakuliahActivityFix.class);
                startActivity(intent);
            }
        });

        btnHapusMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatakuliahActivityFix.this, HapusMatakuliahActivity.class);
                startActivity(intent);
            }
        });

        btnUpdateMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatakuliahActivityFix.this, UpdateMatakuliahActivityFix.class);
                startActivity(intent);
            }
        });
    }
}
