package com.example.progmob_2020.MatakuliahCRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateMatakuliahActivityFix extends AppCompatActivity {
    EditText editTextKodeCari, editTextNamaMatkul, editTextKode, editTextHari, editTextSesi, editTextSks;
    Button btnUbahMatkul;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_matakuliah_fix);

        editTextKodeCari = (EditText)findViewById(R.id.editTextKodeCariFix);
        editTextNamaMatkul = (EditText)findViewById(R.id.editTextNamaMatkulBaruFix);
        editTextKode = (EditText)findViewById(R.id.editTextKodeMatkulBaruFix);
        editTextHari = (EditText)findViewById(R.id.editTextHariBaruFix);
        editTextSesi = (EditText)findViewById(R.id.editTextSesiBaruFix);
        btnUbahMatkul = (Button)findViewById(R.id.btnUbahMatkulFix);
        pd = new ProgressDialog(UpdateMatakuliahActivityFix.this);

        Intent data = getIntent();
        editTextKodeCari.setText(data.getStringExtra("kode"));
        editTextNamaMatkul.setText(data.getStringExtra("nama"));
        editTextKode.setText(data.getStringExtra("kode"));
        editTextHari.setText(data.getStringExtra("hari"));
        editTextSesi.setText(data.getStringExtra("sesi"));
        editTextSks.setText(data.getStringExtra("sks"));
        btnUbahMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Silahkan Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance. getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_Matakuliah(
                        editTextKodeCari.getText().toString(),
                        editTextNamaMatkul.getText().toString(),
                        editTextKode.getText().toString(),
                        editTextHari.getText().toString(),
                        editTextSesi.getText().toString(),
                        editTextSks.getText().toString(),
                        "72180250"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UpdateMatakuliahActivityFix.this,"Data Berhasil di Update",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateMatakuliahActivityFix.this,"Gagal di Update",Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(UpdateMatakuliahActivityFix.this, MainMatakuliahActivityFix.class);
                startActivity(intent);
            }
        });
    }
}