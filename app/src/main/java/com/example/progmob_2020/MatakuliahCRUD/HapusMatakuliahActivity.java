package com.example.progmob_2020.MatakuliahCRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.CRUD.HapusMahasiswaActivity;
import com.example.progmob_2020.CRUD.LihatMahasiswaActivity;
import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HapusMatakuliahActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_matakuliah);

        EditText edKode = (EditText)findViewById(R.id.editTextKodeMatkulFix);
        Button btnHapus = (Button)findViewById(R.id.btnHapus);
        pd = new ProgressDialog(HapusMatakuliahActivity.this);

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_Matakuliah(
                        edKode.getText().toString(),
                        "72180250"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(HapusMatakuliahActivity.this,"Berhasil di Hapus", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(HapusMatakuliahActivity.this, LihatMatakuliahActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(HapusMatakuliahActivity.this, "Gagal", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}