package com.example.progmob_2020.DosenCRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.CRUD.LihatMahasiswaActivity;
import com.example.progmob_2020.CRUD.TambahMahasiswaActivity;
import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_dosen);

        EditText edNama = (EditText)findViewById(R.id.editTextNamaDosen);
        EditText edNim = (EditText)findViewById(R.id.editTextNidn);
        EditText edAlamat = (EditText)findViewById(R.id.editTextAlamatDosen);
        EditText edEmail = (EditText)findViewById(R.id.editTextEmailDosen);
        Button btnSimpan = (Button)findViewById(R.id.buttonTambahDosen);
        pd = new ProgressDialog(TambahDosenActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Loading");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dosen(
                        edNama.getText().toString(),
                        edNim.getText().toString(),
                        edAlamat.getText().toString(),
                        edEmail.getText().toString(),
                        "Kosong",
                        "72180250"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahDosenActivity.this,"Data Berhasil Disimpan",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(TambahDosenActivity.this, LihatDosenActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(TambahDosenActivity.this,"Gagal", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}