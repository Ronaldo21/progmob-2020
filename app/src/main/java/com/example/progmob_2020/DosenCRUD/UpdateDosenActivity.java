package com.example.progmob_2020.DosenCRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.CRUD.MainMhsActivity;
import com.example.progmob_2020.CRUD.UpdateMahasiswaActivity;
import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateDosenActivity extends AppCompatActivity {
    EditText editTextNidnCari, editTextNamaDosen, editTextNidn, editTextAlamatDosen, editTextEmailDosen;
    Button btnUbahDosen;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_dosen);

        editTextNidnCari = (EditText)findViewById(R.id.editTextNidnCariDosen);
        editTextNamaDosen = (EditText)findViewById(R.id.editTextNamaBaruDosen);
        editTextNidn = (EditText)findViewById(R.id.editTextNidnBaruDosen);
        editTextAlamatDosen = (EditText)findViewById(R.id.editTextAlamatBaruDosen);
        editTextEmailDosen = (EditText)findViewById(R.id.editTextEmailBaruDosen);
        btnUbahDosen = (Button)findViewById(R.id.buttonUbahDosen);
        pd = new ProgressDialog(UpdateDosenActivity.this);

        Intent data = getIntent();
        editTextNidnCari.setText(data.getStringExtra("nidn"));
        editTextNamaDosen.setText(data.getStringExtra("nama"));
        editTextNidn.setText(data.getStringExtra("nidn"));
        editTextAlamatDosen.setText(data.getStringExtra("alamat"));
        editTextEmailDosen.setText(data.getStringExtra("email"));

        btnUbahDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Silahkan Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance. getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_dosen(
                        editTextNamaDosen.getText().toString(),
                        editTextNidn.getText().toString(),
                        editTextNidnCari.getText().toString(),
                        editTextAlamatDosen.getText().toString(),
                        editTextEmailDosen.getText().toString(),
                        "Fotonya Diubah",
                        "72180250"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this,"Data Berhasil di Update",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this,"Gagal di Update",Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(UpdateDosenActivity.this, MainDosenActivity.class);
                startActivity(intent);
            }
        });
    }
}