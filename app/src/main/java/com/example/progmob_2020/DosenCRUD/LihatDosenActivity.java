package com.example.progmob_2020.DosenCRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.example.progmob_2020.Adapter.DosenCRUDRecycleAdapter;
import com.example.progmob_2020.Adapter.MahasiswaCRUDRecycleAdapter;
import com.example.progmob_2020.CRUD.LihatMahasiswaActivity;
import com.example.progmob_2020.Model.DosenCRUD;
import com.example.progmob_2020.Model.MahasiswaCRUD;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LihatDosenActivity extends AppCompatActivity {
    RecyclerView rvDosen;
    DosenCRUDRecycleAdapter dosenAdapter;
    ProgressDialog pd;
    List<DosenCRUD> dosenCRUDList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_dosen);
        rvDosen = (RecyclerView)findViewById(R.id.rvGetDosen);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<DosenCRUD>> call = service.getDosen("72180250");

        call.enqueue(new Callback<List<DosenCRUD>>() {
            @Override
            public void onResponse(Call<List<DosenCRUD>> call, Response<List<DosenCRUD>> response) {
                pd.dismiss();
                dosenCRUDList = response.body();
                dosenAdapter = new DosenCRUDRecycleAdapter(dosenCRUDList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(LihatDosenActivity.this);
                rvDosen.setLayoutManager(layoutManager);
                rvDosen.setAdapter(dosenAdapter);
            }

            @Override
            public void onFailure(Call<List<DosenCRUD>> call, Throwable t) {
                Toast.makeText( LihatDosenActivity.this,"Error", Toast.LENGTH_LONG).show();

            }
        });
    }
}