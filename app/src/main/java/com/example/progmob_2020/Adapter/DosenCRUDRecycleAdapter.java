package com.example.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob_2020.Model.DosenCRUD;
import com.example.progmob_2020.R;

import java.util.ArrayList;
import java.util.List;

public class DosenCRUDRecycleAdapter extends  RecyclerView.Adapter<DosenCRUDRecycleAdapter.ViewHolder>{
    private Context context;
    private List<DosenCRUD> dosenCRUDList;

    public DosenCRUDRecycleAdapter(Context context) {
        this.context = context;
        dosenCRUDList = new ArrayList<>();
    }

    public DosenCRUDRecycleAdapter(List<DosenCRUD>dosenCRUDList){
        this.dosenCRUDList=dosenCRUDList;
    }

    public void setDosenList(List<DosenCRUD> dosenCRUDList) {
        this.dosenCRUDList = dosenCRUDList;
        notifyDataSetChanged();
    }

    public List<DosenCRUD> getDosenList() {
        return dosenCRUDList;
    }

    @NonNull
    @Override
    public DosenCRUDRecycleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_list_dosen,parent,false);
        return new DosenCRUDRecycleAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DosenCRUDRecycleAdapter.ViewHolder holder, int position) {
        DosenCRUD d = dosenCRUDList.get(position);

        holder.tvNama.setText(d.getNama());
        holder.tvNidn.setText(d.getNidn());
        holder.tvAlamat.setText(d.getAlamat());
        holder.tvEmail.setText(d.getEmail());
        holder.tvFoto.setText(d.getFoto());

    }

    @Override
    public int getItemCount() {
        return dosenCRUDList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama,tvNidn,tvnotelp, tvAlamat,tvEmail,tvFoto;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvNidn = itemView.findViewById(R.id.tvNidn);
            tvAlamat = itemView.findViewById(R.id.tvAlamat);
            tvEmail = itemView.findViewById(R.id.tvEmail);
            tvFoto = itemView.findViewById(R.id.tvFoto);

        }
    }
}
