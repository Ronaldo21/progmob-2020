package com.example.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob_2020.Model.DosenCRUD;
import com.example.progmob_2020.Model.MatakuliahCRUD;
import com.example.progmob_2020.R;

import java.util.ArrayList;
import java.util.List;

public class MatakuliahCRUDRecycleAdapter extends RecyclerView.Adapter<MatakuliahCRUDRecycleAdapter.ViewHolder> {
    private Context context;
    private List<MatakuliahCRUD> matakuliahCRUDList;

    public MatakuliahCRUDRecycleAdapter(Context context) {
        this.context = context;
        matakuliahCRUDList = new ArrayList<>();

    }

    public MatakuliahCRUDRecycleAdapter(List<MatakuliahCRUD>matakuliahCRUDList){
        this.matakuliahCRUDList=matakuliahCRUDList;
    }

    public void setMatakuliahList(List<MatakuliahCRUD> matakuliahCRUDList) {
        this.matakuliahCRUDList = matakuliahCRUDList;
        notifyDataSetChanged();
    }

    public List<MatakuliahCRUD> getMatakuliahList() {
        return matakuliahCRUDList;
    }

    @NonNull
    @Override
    public MatakuliahCRUDRecycleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_list_matakuliah,parent,false);
        return new MatakuliahCRUDRecycleAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MatakuliahCRUDRecycleAdapter.ViewHolder holder, int position) {
        MatakuliahCRUD m = matakuliahCRUDList.get(position);

        holder.tvNama.setText(m.getNama());
        holder.tvKode.setText(m.getKode());
        holder.tvHari.setText(m.getHari());
        holder.tvSesi.setText(m.getSesi());
        holder.tvSks.setText(m.getSks());

    }

    @Override
    public int getItemCount() {
        return matakuliahCRUDList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama,tvKode, tvHari,tvSesi,tvSks;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNamaMatkul);
            tvKode = itemView.findViewById(R.id.tvKode);
            tvHari = itemView.findViewById(R.id.tvHari);
            tvSesi = itemView.findViewById(R.id.tvSesi);
            tvSks = itemView.findViewById(R.id.tvSks);

        }
    }
}
