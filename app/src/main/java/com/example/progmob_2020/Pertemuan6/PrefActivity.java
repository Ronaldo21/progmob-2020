package com.example.progmob_2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.progmob_2020.R;

public class PrefActivity extends AppCompatActivity {
    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);


        Button btnPrf =(Button)findViewById(R.id.btnPref);

        SharedPreferences pref = PrefActivity.this.getSharedPreferences("prefs_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        //Membaca pref loginn apakah true atau false
        isLogin = pref.getString("isLogin", "0");
        if(isLogin.equals("1"))
        {
            btnPrf.setText("Logout");
        }else
        {
            btnPrf.setText("Login");
        }

        //Pengisian
        btnPrf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLogin = pref.getString("isLogin", "0");
                if(isLogin.equals("0"))
                {
                    editor.commit();
                    editor.putString("isLogin","1");
                    Intent intent = new Intent(PrefActivity.this, BerandaActivity.class);
                    startActivity(intent);
                }

            }
        });
    }
}