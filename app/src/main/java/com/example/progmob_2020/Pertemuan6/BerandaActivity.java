package com.example.progmob_2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.progmob_2020.CRUD.MainMhsActivity;
import com.example.progmob_2020.DosenCRUD.MainDosenActivity;
import com.example.progmob_2020.MatakuliahCRUD.MainMatakuliahActivityFix;
import com.example.progmob_2020.R;

public class BerandaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beranda);

        Button btnDosen = (Button)findViewById(R.id.btnDosen);
        Button btnMahasiswa = (Button)findViewById(R.id.btnMahasiswa);
        Button btnMatakuliah = (Button)findViewById(R.id.btnMatakuliah);
        Button logOut = (Button)findViewById(R.id.btnLogOut);


        btnDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BerandaActivity.this, MainDosenActivity.class);
                startActivity(intent);
            }
        });

        btnMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BerandaActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });

        btnMatakuliah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BerandaActivity.this, MainMatakuliahActivityFix.class);
                startActivity(intent);
            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BerandaActivity.this, PrefActivity.class);
                startActivity(intent);
            }
        });
    }
}