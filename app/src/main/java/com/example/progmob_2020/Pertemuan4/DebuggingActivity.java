package com.example.progmob_2020.Pertemuan4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmob_2020.Adapter.DebuggingRecyclerAdapter;
import com.example.progmob_2020.Model.MahasiswaDebugging;
import com.example.progmob_2020.R;

import java.util.ArrayList;
import java.util.List;


public class DebuggingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debugging);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        DebuggingRecyclerAdapter mahasiswaRecyclerAdapter;

        //data dummy
        List<MahasiswaDebugging> mahasiswaListDebug = new ArrayList<>();

        //generate data mahasiswa
        MahasiswaDebugging m1 = new MahasiswaDebugging("Ronaldo","72180250","21212121");
        MahasiswaDebugging m2 = new MahasiswaDebugging("Ronaldi","72180250","21212121");
        MahasiswaDebugging m3 = new MahasiswaDebugging("Ronalde","72180250","21212121");
        MahasiswaDebugging m4 = new MahasiswaDebugging("Ronaldu","72180250","21212121");
        MahasiswaDebugging m5 = new MahasiswaDebugging("Ronalda","72180250","21212121");

        mahasiswaListDebug.add(m1);
        mahasiswaListDebug.add(m2);
        mahasiswaListDebug.add(m3);
        mahasiswaListDebug.add(m4);
        mahasiswaListDebug.add(m5);

        
        mahasiswaRecyclerAdapter = new DebuggingRecyclerAdapter(DebuggingActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaListDebug);

        rv.setLayoutManager(new LinearLayoutManager(DebuggingActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);
    }
}
